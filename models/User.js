const mongoose = require('mongoose');

const User = mongoose.model(
    "users",
    {
        name:{
            type:String,
            required: true
        },
        email:{
            type:String,
            required: true,
        },
        tel:{
            type:String,
        },
        created_at:{
            type:Date,
            default:Date.now
        }

    }
);

module.exports = {User};