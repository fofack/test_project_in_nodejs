const mongoose = require("mongoose");

const Todo = mongoose.model(
    "todos",
   {
        name: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: false,
        },
        completed: {
            type: Boolean,
            default: false,
            required: true,
        },
        endDate: {
            type: Date,
            required: true,
        },
        user: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            }
        ]
    },
);

module.exports = {Todo};
