const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const port = 3000;

const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');

const swaggerOptions = {
  swaggerDefinition: {
    info:{
      title: 'Library API',
      version:'1.0.0'
    }
  },
  apis: ["./routes/api/*.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs));


require("./database/config");
const userRouter = require("./routes/api/users");
const todoRouter = require("./routes/api/todo");

//middleware
app.use(bodyParser.json());
app.use("/api/users", userRouter);
app.use("/api/todos", todoRouter);

app.listen(port, () => {
  console.log(`Server started: ${port}`);
});
