const mongoose = require("mongoose");

mongoose
  .connect("mongodb://localhost:27017/todos", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("database connected!"))
  .catch((err) => console.error("Error connection", err));
