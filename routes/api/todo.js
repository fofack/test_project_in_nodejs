var express = require('express');
var router = express.Router();
const todoController = require('../../controller/TodoController');


/**
 * @swagger
 * /api/todos:
 *   get:
 *     description: Get all todo
 *     responses:
 *       200:
 *         description: Success
 */
router.get('/', todoController.index);



/**
 * @swagger
 * /api/todos:
 *   post:
 *     description: Create new todo
 *     parameters:
 *      - name: name
 *        description: name of user
 *        in: formData
 *        required: true
 *        type: string
 *      - name: description
 *        description: description of todo
 *        in: formData
 *        required: false
 *        type: string
 *      - name: completed
 *        description: completed of user
 *        in: formData
 *        required: true
 *        type: boolean
 *      - name: endDate
 *        description: end date for todo
 *        in: formData
 *        required: true
 *        type: date
 *     responses:
 *       201:
 *         description: Created
 */
router.post('/', todoController.create);

/* PUT users updating. */
router.put('/:id', todoController.update);

/* GET users . */
router.get('/:id', todoController.show);

/* DELETE users deleting. */
router.delete('/:id', todoController.delete);

/* GET all to do for users . */
router.get('/:id', todoController.findTodoUser);

module.exports = router;
