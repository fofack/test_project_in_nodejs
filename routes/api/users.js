var express = require('express');
var router = express.Router();
const userController = require('../../controller/UserController');

/**
 * @swagger
 * /api/users:
 *   get:
 *     description: Get all users
 *     responses:
 *       200:
 *         description: Success
 */
router.get('/', userController.index);

/**
 * @swagger
 * /api/users:
 *   post:
 *     description: Create new user
 *     parameters:
 *      - name: name
 *        description: name of user
 *        in: formData
 *        required: true
 *        type: string
 *      - name: email
 *        description: email of user
 *        in: formData
 *        required: true
 *        type: string
 *      - name: tel
 *        description: phone of user
 *        in: formData
 *        required: true
 *        type: string
 *     responses:
 *       201:
 *         description: Created
 */
router.post('/', userController.create);


/**
 * @swagger
 * /api/users{id}:
 *   put:
 *     description: Update user
 *     parameters:
 *      - name: name
 *        description: name of user
 *        in: formData
 *        required: true
 *        type: string
 *      - name: email
 *        description: email of user
 *        in: formData
 *        required: true
 *        type: string
 *      - name: tel
 *        description: phone of user
 *        in: formData
 *        required: true
 *        type: string
 *     responses:
 *       201:
 *         description: Updated
 */
router.put('/:id', userController.update);

/* GET users . */
router.get('/:id', userController.show);

/* DELETE users deleting. */
router.delete('/:id', userController.delete);

module.exports = router;
