const { Todo } = require("../models/Todo");
const ObjectID = require("mongoose").Types.ObjectId;

exports.index = (req, res) => {
  Todo.find((err, docs) => {
    if (!err) res.send(docs);
    else console.log("Error to get data:" + err);
  });
};

exports.create = (req, res) => {
  try {
    const todo = new Todo({
      name: req.body.name,
      description: req.body.description,
      endDate: req.body.endDate,
      user: req.body.user,
    });
    todo.save();
    res.status(201).json({ data: todo });
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};

exports.update = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID Unknow :" + req.params.id);

  const todo = {
    name: req.body.name,
    description: req.body.description,
    completed: req.body.completed,
    endDate: req.body.endDate,
    user: req.body.user,
  };

  Todo.findByIdAndUpdate(
    req.params.id,
    { $set: todo },
    { new: true },
    (err, docs) => {
      if (!err) res.status(201).json({ success: true, data: docs });
      else res.status(401).json({ message: err.message });
    }
  );
};

exports.show = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID Unknow :" + req.params.id);
  Todo.findById(req.params.id, (err, docs) => {
    if (!err) res.status(201).json({ success: true, data: docs });
    else res.status(401).json({ message: err.message });
  });
};

exports.delete = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID Unknow :" + req.params.id);

  Todo.findByIdAndRemove(req.params.id, (err, docs) => {
    if (!err) res.status(201).json();
    else res.status(401).json({ message: err.message });
  });
};

exports.findTodoUser = (req, res) => {
  var id = req.params.id;
  const todo = Todo.findOne({
    where: { user: id },
  });
  console.log(todo);
  // });
};
