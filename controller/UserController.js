const { User } = require("../models/User");
const ObjectID = require("mongoose").Types.ObjectId;


exports.index = (req, res) => {
  User.find((err, docs) => {
    if (!err) res.send(docs);
    else console.log("Error to get data:" + err);
  });
};

exports.create = (req, res) => {
  try {
    const user = new User({
      name: req.body.name,
      email: req.body.email,
      tel: req.body.tel,
    });
    user.save();
    res.status(201).json({ success: true, data: user });
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};

exports.update = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID Unknow :" + req.params.id);

  const user = {
    name: req.body.name,
    email: req.body.email,
    tel: req.body.tel,
  };

  User.findByIdAndUpdate(
    req.params.id,
    { $set: user },
    { new: true },
    (err, docs) => {
      if (!err) res.status(201).json({ success: true, data: docs });
      else res.status(401).json({ message: err.message });
    }
  );
};

exports.show = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID Unknow :" + req.params.id);
  User.findById(req.params.id, (err, docs) => {
    if (!err) res.status(201).json({ success: true, data: docs });
    else res.status(401).json({ message: err.message });
  });
};

exports.delete = (req, res) => {
    if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID Unknow :" + req.params.id);

    User.findByIdAndRemove(
        req.params.id,
        (err, docs) => {
            if (!err) res.status(201).json();
            else res.status(401).json({ message: err.message });
        }
    )
}